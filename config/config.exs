# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :fivem,
  ecto_repos: [Fivem.Repo]

# Configures the endpoint
config :fivem, FivemWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "FrVU8q4TdlZFwygqcgDE6EgsrAIN0X5oh+TE/ayuiTK13e31wxqrSSzsGgHRtTII",
  render_errors: [view: FivemWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Fivem.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
