defmodule Fivem.ForumTest do
  use Fivem.DataCase, async: true
  import Fivem.TestHelpers
  alias Fivem.Forum
  alias Forum.Category

  test "can create a category" do
    {:ok, admin_user} = fixture(:create_random_admin_user)

    {:ok, %Category{name: "General", slug: "general"}} =
      Forum.create_category(admin_user, %{
        name: "General",
        slug: "general"
      })
  end

  @tag :skip
  test "can't create a category if I don't have permissions"

  @tag :skip
  test "can't access a category if I don't have permissions"

  @tag :skip
  test "can edit a category"

  @tag :skip
  test "can't edit a category if I don't have permissions"

  @tag :skip
  test "can delete a category"

  @tag :skip
  test "can't delete a category if I don't have permissions"

  @tag :skip
  test "can create a thread"

  @tag :skip
  test "can't create a thread if I don't have permission to"

  @tag :skip
  test "can see a thread"

  @tag :skip
  test "can't see a thread if I don't have permission to"

  @tag :skip
  test "can reply to a thread"

  @tag :skip
  test "can't reply to a thread if I don't have permission to"

  @tag :skip
  test "can't reply to a thread if it's locked and I don't have permission"

  @tag :skip
  test "can reply to a thread if it's locked and I have permission"
end
