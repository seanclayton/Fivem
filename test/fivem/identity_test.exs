defmodule Fivem.IdentityTest do
  use Fivem.DataCase, async: true
  import Fivem.TestHelpers
  alias Fivem.Identity
  alias Identity.User

  test "can create a regular user" do
    assert {:ok,
            %User{
              username: "bob",
              type: "regular"
            }} =
             Identity.create_regular_user(%{
               username: "bob",
               password: "password"
             })
  end

  test "can create an admin user" do
    assert {:ok,
            %User{
              username: "admin-bob",
              type: "admin"
            }} =
             Identity.create_admin_user(%{
               username: "admin-bob",
               password: "password"
             })
  end

  test "can convert a regular user to an admin and vice-versa" do
    {:ok,
     user = %User{
       type: "regular"
     }} =
      Identity.create_regular_user(%{
        username: "bob",
        password: "password"
      })

    assert {:ok, user = %User{username: "bob", type: "admin"}} =
             Identity.convert_regular_to_admin(user)

    assert {:ok, %User{username: "bob", type: "regular"}} =
             Identity.convert_admin_to_regular(user)
  end

  test "can edit the user" do
    {:ok, user} =
      fixture(:create_regular_user, %{
        username: "bob",
        password: "password"
      })

    assert {:ok, %User{username: "bob-new"}} =
             Identity.edit_user(user, %{
               username: "bob-new"
             })
  end

  test "cannot plainly edit password" do
    {:ok, user} =
      fixture(:create_regular_user, %{
        username: "bob",
        password: "password"
      })

    Identity.edit_user(user, %{password: "new-password"})

    refute Comeonin.Argon2.checkpw("new-password", user.password_hash)
  end

  test "can delete a user" do
    {:ok, user} =
      fixture(:create_regular_user, %{
        username: "bob",
        password: "password"
      })

    assert {:ok, _user} = Identity.delete_user(user)
  end

  test "should not be able to see password on user" do
    {:ok, user} =
      fixture(:create_regular_user, %{
        username: "bob",
        password: "password"
      })

    refute user.password
  end

  test "a user can edit themselves" do
    {:ok, user} =
      fixture(:create_regular_user, %{
        username: "bob",
        password: "password"
      })

    assert true = Bodyguard.permit?(Identity, :edit_user, user, user)
  end

  test "a user can delete themselves" do
    {:ok, user} =
      fixture(:create_regular_user, %{
        username: "bob",
        password: "password"
      })

    assert true = Bodyguard.permit?(Identity, :delete_user, user, user)
  end

  test "an admin can edit other users" do
    {:ok, regular_user} =
      fixture(:create_regular_user, %{
        username: "bob",
        password: "password"
      })

    {:ok, admin_user} =
      fixture(:create_admin_user, %{
        username: "admin",
        password: "password"
      })

    assert true = Bodyguard.permit?(Identity, :edit_user, admin_user, regular_user)
  end

  test "a user cannot edit another user" do
    {:ok, user_1} =
      fixture(:create_regular_user, %{
        username: "bob",
        password: "password"
      })

    {:ok, user_2} =
      fixture(:create_regular_user, %{
        username: "bob2",
        password: "password"
      })

    refute Bodyguard.permit?(Identity, :edit_user, user_1, user_2)
  end

  test "a user cannot delete another user" do
    {:ok, user_1} =
      fixture(:create_regular_user, %{
        username: "bob",
        password: "password"
      })

    {:ok, user_2} =
      fixture(:create_regular_user, %{
        username: "bob2",
        password: "password"
      })

    refute Bodyguard.permit?(Identity, :delete_user, user_1, user_2)
  end

  test "can get a user with just their ID" do
    {:ok, user} =
      fixture(:create_regular_user, %{
        username: "bob",
        password: "password"
      })

    {:ok, found_user} = Identity.get_user_by_id(user.id)

    assert user.id == found_user.id
  end

  test "get an error when user ID doesn't exist" do
    assert {:error, :not_found} = Identity.get_user_by_id(9999)
  end
end
