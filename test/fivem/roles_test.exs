defmodule Fivem.RolesTest do
  use Fivem.DataCase, async: true
  import Fivem.TestHelpers
  alias Fivem.Roles
  alias Roles.{Role}

  test "can create a role with default permissions" do
    {:ok, admin} = fixture(:create_random_admin_user)

    assert {:ok,
            %Role{
              name: "Cool role bro",
              can_create_categories: false,
              can_edit_categories: false,
              can_delete_categories: false,
              can_create_threads: true,
              can_edit_other_threads: false,
              can_delete_other_threads: false,
              can_delete_their_threads: true,
              can_reply_to_threads: true,
              can_edit_other_thread_replies: false,
              can_edit_their_thread_replies: true
            }} =
             Roles.create_role(admin, %{
               name: "Cool role bro"
             })
  end

  @tag :skip
  test "can't create a role if I don't have permission"

  @tag :skip
  test "can give a role to a user"

  @tag :skip
  test "can't give a role to a user if I don't have permission"

  @tag :skip
  test "can edit a role"

  @tag :skip
  test "can't edit a role if I don't have permission"
end
