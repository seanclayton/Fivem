defmodule Fivem.TestHelpers do
  @moduledoc """
  Helpers for tests, simple little functions
  that make writing tests a little easier.
  """

  alias Fivem.Identity

  def fixture(:create_regular_user, attrs) do
    Identity.create_regular_user(attrs)
  end

  def fixture(:create_admin_user, attrs) do
    Identity.create_admin_user(attrs)
  end

  def fixture(:create_random_regular_user) do
    create_random_user(:create_regular_user)
  end

  def fixture(:create_random_admin_user) do
    create_random_user(:create_admin_user)
  end

  defp create_random_user(type) do
    fixture(type, %{
      username: Faker.Internet.user_name(),
      password: Faker.String.base64()
    })
  end
end
