defmodule Fivem.Repo.Migrations.AddRolesTable do
  use Ecto.Migration

  def change do
    create table(:roles) do
      add(:name, :string, null: false)
      add(:can_create_categories, :boolean, null: false)
      add(:can_edit_categories, :boolean, null: false)
      add(:can_delete_categories, :boolean, null: false)
      add(:can_create_threads, :boolean, null: false)
      add(:can_edit_other_threads, :boolean, null: false)
      add(:can_delete_other_threads, :boolean, null: false)
      add(:can_delete_their_threads, :boolean, null: false)
      add(:can_reply_to_threads, :boolean, null: false)
      add(:can_edit_other_thread_replies, :boolean, null: false)
      add(:can_edit_their_thread_replies, :boolean, null: false)

      timestamps()
    end
  end
end
