defmodule Fivem.Repo.Migrations.AddUsersTable do
  use Ecto.Migration

  def change do
    create table(:users) do
      add(:username, :string, null: false)
      add(:password_hash, :string, null: false)
      add(:type, :string, null: false)
    end
  end
end
