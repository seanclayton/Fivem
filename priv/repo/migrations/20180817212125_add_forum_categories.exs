defmodule Fivem.Repo.Migrations.AddForumCategories do
  use Ecto.Migration

  def change do
    create table(:forum_categories) do
      add(:name, :citext)
      add(:slug, :citext)
    end
  end
end
