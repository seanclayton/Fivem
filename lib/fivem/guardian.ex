defmodule Fivem.Guardian do
  use Guardian, otp_app: :fivem
  alias Fivem.Identity
  alias Identity.User

  def subject_for_token(%User{} = resource, _claims) do
    {:ok, to_string(resource.id)}
  end

  def subject_for_token(_, _) do
    {:error, :reason_for_error}
  end

  def resource_from_claims(%{"sub" => sub}) do
    {:ok, Identity.get_user_by_id(sub)}
  end

  def resource_from_claims(_claims) do
    {:error, :reason_for_error}
  end
end
