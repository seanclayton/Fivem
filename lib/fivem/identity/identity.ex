defmodule Fivem.Identity do
  @moduledoc """
  Container to interract with users.
  """

  alias Fivem.Repo
  alias Fivem.Identity.{User, Policy}

  defdelegate authorize(action, current_user, user), to: Policy

  def create_regular_user(attrs) do
    %User{}
    |> User.new_regular()
    |> create_user(attrs)
  end

  def create_admin_user(attrs) do
    %User{}
    |> User.new_admin()
    |> create_user(attrs)
  end

  def convert_regular_to_admin(%User{} = user) do
    user
    |> User.convert_regular_to_admin()
    |> Repo.update()
  end

  def convert_admin_to_regular(%User{} = user) do
    user
    |> User.convert_admin_to_regular()
    |> Repo.update()
  end

  def edit_user(%User{} = user, changes) do
    user
    |> User.edit(changes)
    |> Repo.update()
  end

  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  def get_user_by_id(id) do
    case user = Repo.get(User, id) do
      %User{} -> {:ok, user}
      _ -> {:error, :not_found}
    end
  end

  defp create_user(changeset, attrs) do
    changeset
    |> User.new(attrs)
    |> Repo.insert()
  end
end
