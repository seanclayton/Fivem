defmodule Fivem.Identity.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias __MODULE__

  schema "users" do
    field(:username, :string)
    field(:type, :string)
    field(:password, :string, virtual: true)
    field(:password_hash, :string)
  end

  @keys ~w(username password type)a
  def new(changeset, attrs) do
    changeset
    |> cast(attrs, @keys)
    |> validate_required(@keys)
    |> put_password_hash()
  end

  def new_regular(user = %User{}) do
    user
    |> cast(%{}, [])
    |> put_change(:type, "regular")
  end

  def new_admin(user = %User{}) do
    user
    |> cast(%{}, [])
    |> put_change(:type, "admin")
  end

  def convert_regular_to_admin(user = %User{}) do
    user
    |> cast(%{}, [])
    |> put_change(:type, "admin")
  end

  def convert_admin_to_regular(user = %User{}) do
    user
    |> cast(%{}, [])
    |> put_change(:type, "regular")
  end

  @editable_attrs ~w(username)
  def edit(user = %User{}, changes) do
    cast(user, changes, @editable_attrs)
  end

  defp put_password_hash(
         changeset = %Ecto.Changeset{valid?: true, changes: %{password: password}}
       ) do
    change(changeset, Comeonin.Argon2.add_hash(password))
  end

  defp put_password_hash(changeset), do: changeset
end
