defmodule Fivem.Identity.Policy do
  @behaviour Bodyguard.Policy

  alias Fivem.Identity.User

  def authorize(action, current_user, params)

  # An admin can do anything
  def authorize(_action, %User{type: "admin"}, _user), do: true

  # A user can edit or delete themselves
  def authorize(action, %User{id: user_id}, %User{id: user_id})
      when action in [:edit_user, :delete_user],
      do: true

  # Anything else is a no-go
  def authorize(_action, _current_user, _params), do: false
end
