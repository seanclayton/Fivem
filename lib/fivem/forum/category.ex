defmodule Fivem.Forum.Category do
  use Ecto.Schema
  import Ecto.Changeset
  alias __MODULE__

  @moduledoc """
  Forum categories. Threads go in categories.
  """

  schema "forum_categories" do
    field(:name, :string, null: false)
    field(:slug, :string, null: false)
  end

  def create_category(category = %Category{}, attrs) do
    category
    |> cast(attrs, [:name, :slug])
    |> validate_required([:name, :slug])
  end
end
