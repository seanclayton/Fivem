defmodule Fivem.Forum do
  alias Fivem.Repo
  alias __MODULE__

  @moduledoc """
  Container for all public methods to interract with forums.
  """

  # def get_category()
  # def get_all_categories()
  def create_category(_user, attrs) do
    %Forum.Category{}
    |> Forum.Category.create_category(attrs)
    |> Repo.insert()
  end

  # def edit_category()
  # def delete_category()

  # def get_thread()
  # def get_threads_in_category()
  # def create_thread()
  # def edit_thread()
  # def delete_thread()

  # def reply_to_thread()
  # def edit_thread_reply()
  # def delete_thread_reply()
end
