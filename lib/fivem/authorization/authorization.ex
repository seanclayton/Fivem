defmodule Fivem.Authorization do
  # @doc """
  # This essentially takes a user, action, and resource
  # and grabs all roles of the user and the permissions
  # those roles have according to the resource.

  # For example, if a user is trying to create a thread in a category,
  # it'll look like this:

  # can?(%Fivem.User{}, :create_thread, %Fivem.Category{})

  # The `action` param is essentially the name of the permission.

  # And after looking at the roles the user has, and seeing if they are allowed to
  # do the action (we need the resource because that category might have specific
  # per-role allowances and disallowances), we return true, otherwise, return false
  # """
  # can?(%Fivem.User{}, action, resource) :: boolean()
  # def can?(_user, _action, _resource)

  # @doc """
  # Takes an array of roles and returns a
  # map of things this array of roles can and cannot do.

  # It's basically going to reduce the array, and if any
  # role says the user can do something, they automatically
  # get access it it, even if every other role says they can't.

  # It's a one-and-done situation.
  # """
  # def rollup_roles([%Fivem.Roles.Role{}]) :: map()
  # def rollup_roles(roles)
end
