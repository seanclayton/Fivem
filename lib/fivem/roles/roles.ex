defmodule Fivem.Roles do
  alias Fivem.{Repo, Roles}
  alias Roles.Role
  alias Fivem.Identity.User

  @moduledoc """
  Container for interacting with roles.
  """

  @default_permissions [
    can_create_categories: false,
    can_edit_categories: false,
    can_delete_categories: false,
    can_create_threads: true,
    can_edit_other_threads: false,
    can_delete_other_threads: false,
    can_delete_their_threads: true,
    can_reply_to_threads: true,
    can_edit_other_thread_replies: false,
    can_edit_their_thread_replies: true
  ]

  def create_role(%User{}, attrs) do
    attrs = attrs |> merge_permissions

    %Role{}
    |> Role.create_role(attrs)
    |> Repo.insert()
  end

  defp merge_permissions(attrs) do
    @default_permissions
    |> Enum.into(%{})
    |> Map.merge(attrs)
  end

  # def edit_role()
  # def delete_role()

  # def edit_public_role()
  # def edit_member_role()
end
