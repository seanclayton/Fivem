defmodule Fivem.Roles.PublicRole do
  @moduledoc """
  The role applied to users who are not signed into the forum.
  """
end
