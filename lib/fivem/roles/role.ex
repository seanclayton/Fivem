defmodule Fivem.Roles.Role do
  use Ecto.Schema
  import Ecto.Changeset
  alias __MODULE__

  @moduledoc """
  Generic container for _custom_ roles—Not the `public` or `member` roles.
  """

  schema "roles" do
    field(:name, :string, null: false)
    field(:can_create_categories, :boolean, null: false)
    field(:can_edit_categories, :boolean, null: false)
    field(:can_delete_categories, :boolean, null: false)
    field(:can_create_threads, :boolean, null: false)
    field(:can_edit_other_threads, :boolean, null: false)
    field(:can_delete_other_threads, :boolean, null: false)
    field(:can_delete_their_threads, :boolean, null: false)
    field(:can_reply_to_threads, :boolean, null: false)
    field(:can_edit_other_thread_replies, :boolean, null: false)
    field(:can_edit_their_thread_replies, :boolean, null: false)

    timestamps(type: :utc_datetime)
  end

  @permissions [
    :can_create_categories,
    :can_edit_categories,
    :can_delete_categories,
    :can_create_threads,
    :can_edit_other_threads,
    :can_delete_other_threads,
    :can_delete_their_threads,
    :can_reply_to_threads,
    :can_edit_other_thread_replies,
    :can_edit_their_thread_replies
  ]

  @attrs [:name] ++ @permissions

  def create_role(role = %Role{}, attrs) do
    role
    |> cast(attrs, @attrs)
    |> validate_required(@attrs)
  end
end
