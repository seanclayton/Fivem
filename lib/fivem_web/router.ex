defmodule FivemWeb.Router do
  use FivemWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/api", FivemWeb do
    pipe_through(:api)
  end
end
